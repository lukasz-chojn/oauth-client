package oauth.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Klasa konfiguracyjna zabezpieczeń
 */
@EnableWebSecurity
@EnableOAuth2Sso
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final Logger LOGGER = LoggerFactory.getLogger(SecurityConfig.class);

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        LOGGER.info("Konfiguracja zabezpieczeń");
        http.authorizeRequests().antMatchers("/logged").authenticated()
                .and()
                .logout().logoutUrl("/logout").clearAuthentication(true).deleteCookies("JSESSIONID")
                .and()
                .csrf().disable()
                .cors().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        LOGGER.info("Odblokowanie danych statycznych");
        web.ignoring().antMatchers("/js/**, /css/**, /img/**");
    }
}
