package oauth.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Klasa konfiguracyjna widoku
 */
@Configuration
public class AppConfig implements WebMvcConfigurer {

    private final Logger LOGGER = LoggerFactory.getLogger(AppConfig.class);

    @Bean
    public ViewResolver viewResolver() {
        LOGGER.info("Konfiguracja widoku");
        InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
        internalResourceViewResolver.setSuffix(".html");
        internalResourceViewResolver.setCache(false);
        return internalResourceViewResolver;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        LOGGER.info("Konfiguracja danych statycznych");
        registry.addResourceHandler("/resources/**").addResourceLocations("classpath:resources/");
        registry.addResourceHandler("/**").addResourceLocations("classpath:templates/");
        registry.addResourceHandler("/js/**").addResourceLocations("classpath:static/js/");
        registry.addResourceHandler("/img/**").addResourceLocations("classpath:static/img/");
        registry.addResourceHandler("/css/**").addResourceLocations("classpath:static/css/");
    }
}
