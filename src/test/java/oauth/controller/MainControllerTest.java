package oauth.controller;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.LinkedHashMap;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.testng.Assert.assertEquals;

public class MainControllerTest {

    private MockMvc mockMvc;
    @Mock
    private OAuth2Authentication oAuth2Authentication;
    @Mock
    private SecurityContext securityContext;
    @InjectMocks
    private MainController mainController;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
    }

    @Test
    public void testLogged() throws Exception {
        LinkedHashMap testUser = testUser();

        when(securityContext.getAuthentication()).thenReturn(oAuth2Authentication);
        SecurityContextHolder.setContext(securityContext);
        when(oAuth2Authentication.getUserAuthentication()).thenReturn(oAuth2Authentication);
        when(oAuth2Authentication.getUserAuthentication().getDetails()).thenReturn(testUser);

        MockHttpServletRequestBuilder requestBuilder = get("/logged");

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
    }

    @Test
    public void testLogout() throws Exception {
        when(securityContext.getAuthentication()).thenReturn(oAuth2Authentication);
        SecurityContextHolder.setContext(securityContext);

        MockHttpServletRequestBuilder requestBuilder = get("/logout");

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
    }

    private LinkedHashMap<String, String> testUser() {
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        User user = new User("user", "pass", Arrays.asList(new SimpleGrantedAuthority("user"), new SimpleGrantedAuthority("admin")));
        map.put("username", user.getUsername());
        map.put("roles", user.getAuthorities().toString());
        return map;
    }
}