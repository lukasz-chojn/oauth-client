package oauth.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;

/**
 * Główny kontroler aplikacji
 */
@RestController
public class MainController {

    private final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @GetMapping("/logged")
    public String logged() {
        Object details = ((OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication()).getUserAuthentication().getDetails();
        LOGGER.info("Logged in as " + ((LinkedHashMap) details).values().toArray()[0].toString());
        return ((LinkedHashMap) details).values().toArray()[0].toString();
    }

    @GetMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null) {
            LOGGER.info("Logged out successfully");
            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }
    }
}
